Pod::Spec.new do |s|

  s.name         = 'FeedbackKit'
  s.version      = '0.1'
  s.summary      = 'All-in-one approach for feedback, console and file logging, and analytics.'

  s.description  = <<-DESC
	All-in-one approach for feedback, console and file logging, and analytics.
                   DESC

  s.homepage     = 'https://bitbucket.org/mohamed_afsar/feedbackkit/src/master/'

  s.license      = { :type => 'Private', :file => 'LICENSE' }

  s.author             = { 'mohamed.afsar' => 'afsar@freshworks.io' }
  
  s.source       = { :git => 'https://mohamed_afsar@bitbucket.org/mohamed_afsar/feedbackkit.git', :tag => '0.1' }

  s.subspec 'Core' do |cs|
    cs.source_files = 'FeedbackKit/SDK/**/*.{swift}'
  end
   
  s.requires_arc = true

  s.swift_version = '5'

  s.ios.deployment_target = '13.0'

  s.default_subspec = 'Core'

end