//
//  FileLogger.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public protocol FileLogger {
    func log(_ message: String)
    /// Writes buffer logs into the file immediately.
    func flush(withCompletion completion: (() -> Void)?)
}
