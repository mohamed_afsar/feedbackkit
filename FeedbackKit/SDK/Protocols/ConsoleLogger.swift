//
//  ConsoleLogger.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public protocol ConsoleLogger {
    func log(_ message: String)
}
