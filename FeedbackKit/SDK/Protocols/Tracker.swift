//
//  Tracker.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public protocol Tracker {
    func track(eventName: String, params: [String: Any?]?, groupName: String?)
}
