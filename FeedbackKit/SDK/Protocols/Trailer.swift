//
//  Trailer.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 18/02/21.
//

import Foundation

public protocol Trailer {
    func trail(_ event: Trailable, filePath: String, function: String, line: Int, date: Date, trail: Bool)
}

public extension Trailer {
    func convenienceTrail(_ event: Trailable, filePath: String = #file, function: String = #function, line: Int = #line, date: Date = Date(), trail: Bool) {
        self.trail(event, filePath: filePath, function: function, line: line, date: date, trail: trail)
    }
}
