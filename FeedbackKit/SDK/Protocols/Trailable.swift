//
//  Trailable.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public protocol Trailable {
    var eventName: String { get }
    var type: TrailType { get }
    
    var logParams: [String: Any?]? { get }
    
    var trackEventName: String? { get }
    var trackParams: [String: Any?]? { get }
    var trackGroupName: String? { get }
}

public enum TrailType {
    case info
    case warning
    case success
    case failure
        
    func indicator() -> Character {
        switch self {
        case .info: return "ℹ️"
        case .success: return "✅"
        case .failure: return "❌"
        case .warning: return "⚠️"
        }
    }
}
