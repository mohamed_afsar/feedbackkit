//
//  Constants.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal struct Constants {
    struct Format {
        static let logDate = "yyyy-MM-dd HH:mm:ss" // NO I18N
    }
    
    struct SDK {
        static let name = "FeedbackKit" // NO I18N
        static let id = "io.freshworks.ios.FeedbackKit" // NO I18N
    }
    
    struct QueueName {
        static let serial = "serial" // NO I18N
        static let concurrent = "concurrent" // NO I18N
    }
    
    struct DirectoryName {
        static let logs = "FeedbackKitLogs" // NO I18N
    }
    
    struct FilePrefix {
        static let logs = "Logs" // NO I18N
    }
    
    struct FileExtension {
        static let logs = "txt" // NO I18N
    }
    
    struct MIMEType {
        static let logs = "text/plain" // NO I18N
    }
}
