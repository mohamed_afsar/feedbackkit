//
//  RepeatingTimer.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

// https://medium.com/over-engineering/a-background-repeating-timer-in-swift-412cecfd2ef9
internal final class RepeatingTimer {
    // MARK: Internal IVars
    internal var eventHandler: (() -> Void)?
    
    // MARK: Internal ICons
    let timeInterval: TimeInterval
    let timerQueue: DispatchQueue?
    
    // MARK: Private ICons
    private let _sQueue: DispatchQueue
    
    // MARK: Private IVars
    private lazy var _timer: DispatchSourceTimer = {
        let t = DispatchSource.makeTimerSource(queue: self.timerQueue)
        t.schedule(deadline: .now() + self.timeInterval, repeating:                             self.timeInterval)
        t.setEventHandler(handler: { [weak self] in
            guard let strongSelf = self, !strongSelf._isDeinitialized else { return }
            strongSelf.eventHandler?()
        })
        return t
    }()
    private var _state: RepeatingTimer.State = .suspended
    private var _isDeinitialized = false
    
    // MARK: Initialization
    init(timeInterval: TimeInterval, timerQueue: DispatchQueue?, resSusSerialQueue: DispatchQueue?) {
        self.timeInterval = timeInterval
        self.timerQueue = timerQueue
        self._sQueue = resSusSerialQueue ?? RepeatingTimer._newSerialQueue()
    }
    
    func resume() {
        self._sQueue.async { [weak self] in
            guard let strongSelf = self, !strongSelf._isDeinitialized else { return }
            strongSelf._unsafeResume()
        }
    }
    
    func suspend() {
        self._sQueue.async { [weak self] in
            guard let strongSelf = self, !strongSelf._isDeinitialized else { return }
            guard strongSelf._state != .suspended else { return }
            strongSelf._state = .suspended
            strongSelf._timer.suspend()
        }
    }
    
    deinit {
        self._isDeinitialized = true
        self._timer.setEventHandler { }
        self._timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here
         https://forums.developer.apple.com/thread/15902
         */
        self._unsafeResume()
        self.eventHandler = nil
    }
}

// MARK: Helper Functions
private extension RepeatingTimer {
    func _unsafeResume() {
        guard self._state != .resumed else { return }
        self._state = .resumed
        self._timer.resume()
    }
    
    // Static Functions
    static func _newSerialQueue() -> DispatchQueue {
        let label = Constants.SDK.id + String.FK_DOT + "\(RepeatingTimer.self)" + String.FK_DOT + Constants.QueueName.serial + String.FK_DOT +
            "\(Date().timeIntervalSince1970)" // NO I18N
        
        return DispatchQueue(label: label, qos: .userInitiated)
    }
}

// MARK: Custom Types Declaration
internal extension RepeatingTimer {
    // MARK: Enums
    private enum State {
        case suspended
        case resumed
    }
}
