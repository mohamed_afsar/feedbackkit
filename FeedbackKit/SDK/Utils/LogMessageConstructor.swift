//
//  LogMessageConstructor.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal final class LogMessageConstructor {
    // MARK: Private Static Cons
    private let dFrmtr: DateFormatter = {
        let fmtr = DateFormatter()
        fmtr.dateFormat = Constants.Format.logDate
        return fmtr
    }()
}

// MARK: Internal Functions
internal extension LogMessageConstructor {
    func message(_ message: Any, msgParams: [String: Any?]?, prefix: Character, filePath: String, function: String, line: Int, date: Date) -> String {
        
        let fileName = filePath.components(separatedBy: String.FK_FORWARD_SLASH).last ?? String.FK_EMPTY
        
        let logPrefix = "㏒" + // NO I18N
                        String.FK_SPACE +
                        String(prefix) +
                        String.FK_SPACE +
                        self.dFrmtr.fk_microsecondPrecisionString(from: date) +
                        String.FK_SPACE
        
        let fileMeta = fileName +
                        String.FK_COLON +
                        String.FK_SPACE +
                        function +
                        String.FK_SPACE
        
        let lineMeta = "in line:" + // NO I18N
                        String.FK_SPACE +
                        String(line) +
                        String.FK_SPACE +
                        String.FK_HYPHEN +
                        String.FK_SPACE
        
        let paramsStr = msgParams?.fk_logDescription ?? String.FK_EMPTY
        
        return logPrefix + fileMeta + lineMeta + "\(message)" + paramsStr // NO I18N
    }
}
