//
//  BumpSensor.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 22/02/21.
//

import Foundation
import CoreMotion

internal final class BumpSensor {
    // MARK: Internal IVars
    internal var onDeviceShake: (() -> Void)?
    
    // MARK: Private Static ICons
    private static let _kAccelerationThreshold = 1.5
    private static let _kDeviceMotionUpdateInterval = 0.5
    
    // MARK: Private ICons
    private let _motionManager: CMMotionManager = {
        let manager = CMMotionManager()
        manager.deviceMotionUpdateInterval = BumpSensor._kDeviceMotionUpdateInterval
        return manager
    }()
    
    private let _motionUpdatesOpnQ: OperationQueue = {
        let q = OperationQueue()
        q.qualityOfService = .utility
        q.maxConcurrentOperationCount = 1
        return q
    }()
    
    // MARK: Private IVars
    private var _handlingShake = false
}

// MARK: Internal Functions
extension BumpSensor {
    func resume() {
        _motionManager.startDeviceMotionUpdates(to: _motionUpdatesOpnQ) { [weak self] (motion, error) in
            guard let _self = self else { return }
            
            if let userAcceleration = motion?.userAcceleration {
                if (fabs(userAcceleration.x) > BumpSensor._kAccelerationThreshold
                    || fabs(userAcceleration.y) > BumpSensor._kAccelerationThreshold
                    || fabs(userAcceleration.z) > BumpSensor._kAccelerationThreshold) {
                    
                    guard !_self._handlingShake else { return }
                    _self._handlingShake = true
                    _self.onDeviceShake?()
                    _self._handlingShake = false
                }

            } else {
                if let error = error {
                    print("Motion error: \(error)")
                }
                else {
                    print("Unknown Motion error")
                }
            }
        }
    }
    
    func suspend() {
        _motionManager.stopDeviceMotionUpdates()
    }
}
