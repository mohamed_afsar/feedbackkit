//
//  DictionaryExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal extension Dictionary where Value == Any? {
    var fk_logDescription: String {
        var desc = "" // NO I18N
        self.forEach {
            desc += String.FK_NEW_LINE
            desc += "\($0.key)"
            desc += String.FK_COLON
            desc += String.FK_SPACE
            desc += $0.value.fk_description
        }
        return desc
    }
}
