//
//  UIWindowSceneExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 22/02/21.
//

import UIKit

internal extension UIWindowScene {
    static var focused: UIWindowScene? {
        return UIApplication.shared.connectedScenes
            .first { $0.activationState == .foregroundActive && $0 is UIWindowScene } as? UIWindowScene
    }
}
