//
//  FileManagerExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal extension FileManager {
    var fk_logsFileName: String {
        Constants.FilePrefix.logs + String.FK_DOT + Constants.FileExtension.logs
    }
    
    func fk_getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func fk_getLogsDirectory() -> URL {
        return fk_getDocumentsDirectory().appendingPathComponent(Constants.DirectoryName.logs)
    }
    
    func fk_getLogsFileUrl() -> URL {
        return fk_getLogsDirectory().appendingPathComponent(fk_logsFileName)
    }
    
    func fk_createLogsDirIfNeeded() {
        let logsDirPath = fk_getLogsDirectory()
        if !FileManager.default.fileExists(atPath: logsDirPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: logsDirPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Error creating directory: \(error.localizedDescription)") // NO I18N
            }
        }
    }
}
