//
//  NotificationNameExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 24/02/21.
//

import Foundation

public extension Notification.Name {
    static let fk_didSendFeedback = Notification.Name("fk_didSendFeedback")
    static let fk_didFailToSendFeedback = Notification.Name("fk_didFailToSendFeedback")
}
