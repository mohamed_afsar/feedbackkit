//
//  DateFormatterExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal extension DateFormatter {
    // https://stackoverflow.com/a/43130186
    func fk_microsecondPrecisionString(from date: Date, withFormat frmt: String = Constants.Format.logDate) -> String {
        
        if self.dateFormat != frmt {
            self.dateFormat = frmt
        }
        
        let components = calendar.dateComponents(Set([Calendar.Component.nanosecond]), from: date)
        
        let nanosecondsInMicrosecond = Double(1000)
        let microseconds = lrint(Double(components.nanosecond!) / nanosecondsInMicrosecond)
        
        // Subtract nanoseconds from date to ensure string(from: Date) doesn't attempt faulty rounding.
        let updatedDate = calendar.date(byAdding: .nanosecond, value: -(components.nanosecond!), to: date)!
        let dateTimeString = self.string(from: updatedDate)
        
        let string = String(format: "%@.%06ld", // NO I18N
                            dateTimeString,
                            microseconds)
        
        return string
    }
}
