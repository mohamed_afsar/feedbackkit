//
//  OptionalExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal extension Optional {
    var fk_description: String {
        guard let val = self else { return String(describing: self) }
        return String(describing: val)
    }
}
