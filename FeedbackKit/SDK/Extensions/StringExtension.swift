//
//  StringExtension.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal extension String {
    // MARK: Static Cons
    static let FK_EMPTY = "" // NO I18N
    static let FK_FORWARD_SLASH = "/" // NO I18N
    static let FK_SPACE = " " // NO I18N
    static let FK_COLON = ":" // NO I18N
    static let FK_HYPHEN = "-" // NO I18N
    static let FK_NEW_LINE = "\n" // NO I18N
    static let FK_DOT = "." // NO I18N
}
