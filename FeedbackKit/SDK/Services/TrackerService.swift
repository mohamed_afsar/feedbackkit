//
//  TrackerService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal final class TrackerService {
    // TODO: Tracking implementation
}

// MARK: Tracker Conformance
extension TrackerService: Tracker {
    func track(eventName: String, params: [String : Any?]?, groupName: String?) {
        // TODO: Tracking implementation
    }
}
