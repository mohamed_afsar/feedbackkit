//
//  EmailService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 20/02/21.
//

import Foundation
import MessageUI

internal final class EmailService: NSObject {
    // MARK: Internal IVars
    internal var onEvent: ((Event) -> Void)?
    
    // MARK: Private ICons
    private let _recipients: [String]
    private let _subject: String
    private let _body: String
    private let _attachment: Attachment?
    
    // MARK: Private IVars
    private lazy var _composeVC: MFMailComposeViewController = {
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = self
        vc.setToRecipients(_recipients)
        vc.setSubject(_subject)
        vc.setMessageBody(_body, isHTML: false)
        if let attachment = _attachment {
            vc.addAttachmentData(attachment.data, mimeType: attachment.mimeType, fileName: attachment.fileName)
        }
        return vc
    }()
    
    convenience override init() {
        fatalError("Unsupported Initializer") // NO I18N
    }
    
    init(recipients: [String], subject: String, body: String, attachment: Attachment?) {
        _recipients = recipients
        _subject = subject
        _body = body
        _attachment = attachment
        super.init()
    }
}

// MARK: Internal Functions
internal extension EmailService {
    func presentClient(from vc: UIViewController) {
        DispatchQueue.main.async {
            self._presentClient(from: vc)
        }
    }
    
    func dismissClient(completion: (() -> Void)?) {
        DispatchQueue.main.async {
            self._dismissClient(completion: completion)
        }
    }
}

// MARK: MFMailComposeViewControllerDelegate Conformance
extension EmailService: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        print("mailComposeController: didFinishWith result: \(result); error: \(error)")
        
        switch result {
        case .cancelled:
            self.onEvent?(.didCancel)
        case .saved:
            self.onEvent?(.didSave)
        case .sent:
            self.onEvent?(.didSend)
        case .failed:
            self.onEvent?(.didFail(error))
        @unknown default:
            self.onEvent?(.unknownClientFinish)
        }
    }
}

// MARK: Helper Functions
private extension EmailService {
    func _presentClient(from vc: UIViewController) {
        if MFMailComposeViewController.canSendMail() {
            vc.present(_composeVC, animated: true, completion: nil)
        }
        else {
            // TODO: Show third party email composer if default Mail app is not present
            print("Can't send email") // NO I18N
            self.onEvent?(.cantSendEMail)
        }
    }
    
    func _dismissClient(completion: (() -> Void)?) {
        guard _composeVC.presentingViewController != nil else {
            completion?()
            return
        }
        _composeVC.dismiss(animated: true, completion: completion)
    }
}

// MARK: Interbal Types
internal extension EmailService {
    enum Event {
        case cantSendEMail
        case didCancel
        case didSave
        case didSend
        case didFail(Error?)
        case unknownClientFinish
    }
    
    struct Attachment {
        var data: Data
        var mimeType: String
        var fileName: String
    }
}
