//
//  ConsoleLogService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

/// This may look trivial, but in future we can enhance this one with Swift-log / NSLog / whole other implementation.
internal final class ConsoleLogService: ConsoleLogger {
    // MARK: Internal Functions
    func log(_ message: String) {
        print(message)
    }
}
