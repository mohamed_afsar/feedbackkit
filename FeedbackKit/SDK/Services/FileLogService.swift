//
//  FileLogService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import UIKit

internal final class FileLogService {
    // MARK: Private Static ICons
    private static let kLogInterval: TimeInterval = 30 // Seconds
    private static let kLogsThresholdCount = 100
    
    // MARK: Private ICons
    private let _sQueue: DispatchQueue = {
        let label = Constants.SDK.id + String.FK_DOT + "\(FileLogService.self)" + String.FK_DOT + Constants.QueueName.serial // NO I18N
        return DispatchQueue(label: label, qos: .utility)
    }()
    private lazy var _logsTimer: RepeatingTimer = {
        let timer = RepeatingTimer(timeInterval: FileLogService.kLogInterval, timerQueue: _sQueue, resSusSerialQueue: _sQueue)
        timer.eventHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf._write(strongSelf._logs, to: strongSelf._fileUrl)
            strongSelf._logs.removeAll()
        }
        return timer
    }()
    
    // MARK: Private IVars
    private var _logs = [String]()
    private var _fileUrl: URL {
        FileManager.default.fk_getLogsFileUrl()
    }
    
    init() {
        _setup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: FileLogger Conformance
extension FileLogService: FileLogger {
    func log(_ message: String) {
        _sQueue.async {
            self._log(message)
        }
    }
    
    func flush(withCompletion completion: (() -> Void)?) {
        _sQueue.async {
            self._write(self._logs, to: self._fileUrl)
            self._logs.removeAll()
            completion?()
        }
    }
}

// MARK: Helper Functions
private extension FileLogService {
    func _setup() {
        FileManager.default.fk_createLogsDirIfNeeded()
        _logsTimer.resume()
        NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: nil) { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.flush(withCompletion: nil)
        }
    }
    
    func _log(_ message: String) {
        _logs.append(message)
        if _logs.count >= FileLogService.kLogsThresholdCount {
            _write(_logs, to: _fileUrl)
            _logs.removeAll()
        }
    }
    
    func _write(_ logs: [String], to url: URL) {
        guard logs.count > 0 else { return }
        let writingStr = logs.joined(separator: String.FK_NEW_LINE)
        do {
            let handle = try FileHandle(forWritingTo: url)
            handle.seekToEndOfFile()
            handle.write(writingStr.data(using: .utf8)!)
            handle.closeFile()
        } catch {
            print("FileHandle: catch: error: \(error.localizedDescription)") // NO I18N
            do {
                try writingStr.data(using: .utf8)?.write(to: url)
            } catch {
                print("Data: write: catch: error: \(error.localizedDescription)") // NO I18N
            }
        }
        #warning("remove this log")
        print("url.path: \(url.path)")
    }
}
