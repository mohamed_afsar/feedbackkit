//
//  TrailService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

internal final class TrailService {
    // MARK: Private ICons
    private let _sQueue: DispatchQueue = {
        let label = Constants.SDK.id + String.FK_DOT + "\(TrailService.self)" + String.FK_DOT + Constants.QueueName.serial // NO I18N
        return DispatchQueue(label: label, qos: .utility)
    }()
    private let _consoleLogger: ConsoleLogger
    private let _fileLogger: FileLogger
    private let _tracker: Tracker
    private let _lMsgConstructor = LogMessageConstructor()
    private let _bumpSensor = BumpSensor()
    private let _feedbackUIService = FeedbackUIService()
    
    // MARK: Initialization
    init(consoleLogger: ConsoleLogger, fileLogger: FileLogger, tracker: Tracker) {
        #warning("TODO: Handle the removal properly")
        try? FileManager.default.removeItem(at: FileManager.default.fk_getLogsFileUrl())
        
        self._consoleLogger = consoleLogger
        self._fileLogger = fileLogger
        self._tracker = tracker
        self._setup()
    }
}

// MARK: Trailer Conformance
extension TrailService: Trailer {
    func trail(_ event: Trailable, filePath: String, function: String, line: Int, date: Date, trail: Bool) {
        guard trail else { return }
        _sQueue.async {
            self._trail(event, filePath: filePath, function: function, line: line, date: date, msgConstructor: self._lMsgConstructor, consoleLogger: self._consoleLogger, fileLogger: self._fileLogger, tracker: self._tracker)
        }
    }
}

// MARK: Helper Functions
private extension TrailService {
    func _setup() {
        _feedbackUIService.onEvent = { [weak self] (event) in
            guard let _self = self else { return }
            switch event {
            case .feedbackClientRequest:
                _self._fileLogger.flush {
                    _self._feedbackUIService.showFeedbackEMailClient()
                }
                
            case .requestToEndService:
                _self._feedbackUIService.suspend()
                _self._bumpSensor.resume()
            }
        }
        
        _bumpSensor.onDeviceShake = { [weak self] in
            guard let _self = self else { return }
            print("_bumpSensor.onDeviceShake")
            _self._bumpSensor.suspend()
            _self._feedbackUIService.resume()
        }
        _bumpSensor.resume()
    }
    
    func _trail(_ event: Trailable, filePath: String, function: String, line: Int, date: Date, msgConstructor: LogMessageConstructor, consoleLogger: ConsoleLogger, fileLogger: FileLogger, tracker: Tracker) {
        
        let msg = msgConstructor.message(event.eventName, msgParams: event.logParams, prefix: event.type.indicator(), filePath: filePath, function: function, line: line, date: date)

        consoleLogger.log(msg)
        fileLogger.log(msg)
        if let trackEventName = event.trackEventName {
            tracker.track(eventName: trackEventName, params: event.trackParams, groupName: event.trackGroupName)
        }
    }
}
