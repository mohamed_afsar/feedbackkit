//
//  FeedbackUIService.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 22/02/21.
//

import UIKit

internal final class FeedbackUIService {
    // MARK: Internal IVars
    internal var onEvent: ((Event) -> Void)?
    
    // MARK: Private IVars
    private var _popUpWindow: UIWindow?
    private var _consentAlertSheetController: UIAlertController?
    private var _emailService: EmailService?
}

// MARK: Internal Functions
internal extension FeedbackUIService {
    func resume() {
        DispatchQueue.main.async {
            self._resume()
        }
    }
    
    func suspend() {
        DispatchQueue.main.async {
            self._suspend()
        }
    }
    
    func showFeedbackEMailClient() {
        DispatchQueue.main.async {
            self._showFeedbackEMailClient()
        }
    }
}

// MARK: Helper Functions
private extension FeedbackUIService {
    func _resume() {
        _ensurePopUpWindow()
        if let _popUpWindow = _popUpWindow {
            _consentAlertSheetController = _newConsentAlertSheetController(from: _popUpWindow.rootViewController!)
            _popUpWindow.rootViewController!.present(_consentAlertSheetController!, animated: true, completion: nil)
            
        }
        else {
            print("Custome window creation failed") // NO I18N
        }
    }
    
    func _suspend() {
        _consentAlertSheetController = nil
        _popUpWindow = nil
    }
    
    func _ensurePopUpWindow() {
        guard _popUpWindow == nil, let windowScene = UIWindowScene.focused else { return }
        _popUpWindow = UIWindow(windowScene: windowScene)
        _popUpWindow?.frame = UIScreen.main.bounds
        _popUpWindow?.backgroundColor = .clear
        _popUpWindow?.windowLevel = UIWindow.Level.statusBar + 1
        let rootVC = UIViewController()
        rootVC.view.backgroundColor = .clear
        _popUpWindow?.rootViewController = rootVC
        _popUpWindow?.makeKeyAndVisible()
    }
    
    func _newConsentAlertSheetController(from viewController: UIViewController) -> UIAlertController {
        let alertController = UIAlertController(title: "Hey!, it is shaky in here...", message: "Do you want to let us know about anything?", preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = viewController.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: viewController.view.bounds.size.width / 2.0, y: viewController.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        alertController.addAction(UIAlertAction(title: "Yes, I am.", style: .default, handler: { [weak self] (action) in
            self?.onEvent?(.feedbackClientRequest)
        }))
        alertController.addAction(UIAlertAction(title: "Nope, all good.", style: .cancel, handler: { [weak self] (action) in
            self?.onEvent?(.requestToEndService)
        }))
        return alertController
    }
    
    func _newEMailService() -> EmailService {
        let recipients = ["afsar@freshworks.io"]
        let subject = "Feedback"
        let body = "Hi Team,\nI just wanted to bring an issue to your notice. Hope the attachment helps in resolving."
        
        var attachment: EmailService.Attachment?
        if let data = try? Data(contentsOf: FileManager.default.fk_getLogsFileUrl()) {
            attachment = EmailService.Attachment(data: data, mimeType: Constants.MIMEType.logs, fileName: FileManager.default.fk_logsFileName)
        }
        
        let service = EmailService(recipients: recipients, subject: subject, body: body, attachment: attachment)
        return service
    }
    
    func _setupCallbacks(service: EmailService) {
        service.onEvent = { [weak self] (event) in
            guard let _self = self else { return }
            switch event {
            case .cantSendEMail, .didCancel, .didSave, .didSend, .didFail(_), .unknownClientFinish:

                if case .didSend = event {
                    NotificationCenter.default.post(name: .fk_didSendFeedback, object: nil)
                }
                
                if case .didFail(_) = event {
                    NotificationCenter.default.post(name: .fk_didFailToSendFeedback, object: nil)
                }
                
                print("Email service callback!")
                _self._emailService?.dismissClient(completion: {
                    _self._emailService = nil
                    _self.onEvent?(.requestToEndService)
                })
            }
        }
    }
    
    func _showFeedbackEMailClient() {
        print("_showFeedbackEMailClient")
        _emailService = _newEMailService()
        _setupCallbacks(service: _emailService!)
        if let rootVC = _popUpWindow?.rootViewController {
            _emailService?.presentClient(from: rootVC)
        }
        else {
            print("Failed to show EMail Client.")
        }
    }
}

// MARK: Interbal Types
internal extension FeedbackUIService {
    enum Event {
        case feedbackClientRequest
        case requestToEndService
    }
}
