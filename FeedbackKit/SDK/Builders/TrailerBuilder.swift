//
//  TrailerBuilder.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public final class TrailerBuilder {
    // MARK: Private ICons
    private var _consoleLogger: ConsoleLogger!
    private var _fileLogger: FileLogger!
    private var _tracker: Tracker!
    
    public init() { }
}

// MARK: Public Functions
public extension TrailerBuilder {
    func with(consoleLogger: ConsoleLogger) -> TrailerBuilder {
        _consoleLogger = consoleLogger
        return self
    }
    
    func with(fileLogger: FileLogger) -> TrailerBuilder {
        _fileLogger = fileLogger
        return self
    }
    
    func with(tracker: Tracker) -> TrailerBuilder {
        _tracker = tracker
        return self
    }
    
    func build() -> Trailer {
        return TrailService(consoleLogger: _consoleLogger, fileLogger: _fileLogger, tracker: _tracker)
    }
}
