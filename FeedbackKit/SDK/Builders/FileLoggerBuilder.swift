//
//  FileLoggerBuilder.swift
//  FeedbackKit
//
//  Created by Mohamed Afsar on 19/02/21.
//

import Foundation

public final class FileLoggerBuilder {
    public init() { }
    
    public func build() -> FileLogger {
        return FileLogService()
    }
}
